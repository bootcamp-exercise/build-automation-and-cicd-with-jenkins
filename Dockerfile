FROM node:18-alpine3.17
RUN mkdir -p /opt/app
COPY app/*  /opt/app
WORKDIR /opt/app
RUN npm install
EXPOSE 3000
CMD ["node", "server.js"]