<details>
<summary>Prerequisites before starting the exercises.</summary>
</br>

```sh
create a droplet for jenkins server.
install jenkins using docker.
docker run -d --name myjenkins -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home jenkins/jenkins:lts

open 8080 port on droplet 
{ip-address}:8080 -- > will open jenkins 

to get the password:
docker ps on droplet server
docker exec -it container_id /bin/bash
cat /var/jenkins_home/secrets/initialAdminPassword --> initial password


install nodejs and npm on jenkins server
docker exec -it -u 0 container_id /bin/bash
apt update
apt install nodejs
apt install npm


For docker to be available on jenkins, attach docker volume from droplet server.
Stop the currently running jenkins container using 
docker stop container_id

#Attach volume
docker run -d --name mynewjenkins -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock jenkins/jenkins:lts

#login as root user and run these below commands.
curl https://get.docker.com/ > dockerinstall && chmod 777 dockerinstall && ./dockerinstall
# modify docker socket file permissions
chmod 666 /var/run/docker.sock

exit
#login without root user
docker exed -it jenkins_container_id /bin/bash


```
</details>

*******

</details>
<details>
<summary>EXERCISE 1: Dockerize your NodeJS App</summary>
</br>



```sh
FROM node:18-alpine3.17
RUN mkdir -p /opt/app
COPY app/*  /opt/app
WORKDIR /opt/app
RUN npm install
EXPOSE 3000
CMD ["node", "server.js"]

```

</details>

******

<details>
<summary>EXERCISE 2: Create a full pipeline for your NodeJS App,Increment version,run tests,Build docker image with incremented version,Push to Docker repository and commit to Git

</summary>

```sh
Steps to Follow below making code changes.
1.Configure credentials in Jenkins for docker repository and gitlab credentials(Use usernamePassword type of credentials)
2.Configure Node tool in Jenkins configuration(name used is node, the same name will be referenced in jenkinsfile)
3.Install binding plugin to store user and password
4.Install Pipeline Utility Steps plugin for reading from package.json

```

```sh
pipeline {
  agent any
  # configure nodejs in tools section in jenkins server. 
  tools {
    nodejs "nodejs"
  }
  stages {
     # Increment version of package.json to minor version, read package.json and append the build_number to form a image_name
    stage("Increment version") {
        steps {
          script {
            //enter app dir where package.json is located
            dir("app") {
              sh "npm version minor"
              def packageJSON = readJSON file: 'package.json'
              def packageJSONVersion = packageJSON.version
              env.IMAGE_NAME = "$packageJSONVersion-$BUILD_NUMBER"
            }
          }
        }
      }
       # Install npm and run tests.
      stage("Test") {
        steps {
          dir("app") {
            sh 'npm install'
            sh "npm test"
          }
        }
      }
    stage("Build and Push image") {
      steps {
        withCredentials([
          usernamePassword(credentialsId: 'soni-dockerhub', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')
        ]) {

          sh "docker build -t sathyavathi11/nodejs:${IMAGE_NAME} ."
          sh "echo $PASSWORD docker login - u $USERNAME --password-stdin"
          sh "docker push sathyavathi11/nodejs:${IMAGE_NAME}"
        }
      }
    }

    stage('commit version update') {
      steps {
        script {
          withCredentials([
            usernamePassword(credentialsId: 'soni-jenkins', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')
          ]) {
            sh 'git config --global user.email "sathyavathi.da@gmail.com"'
            sh 'git config --global user.name "soni karthik"'
            sh 'git remote set-url origin https://$USERNAME:$PASSWORD@https://gitlab.com/bootcamp-exercise/build-automation-and-cicd-with-jenkins.git'
            sh 'git add .'
            sh 'git commit -m "Version update commit"'
            sh 'git push origin HEAD:master'
          }
        }
      }
    }

  }
}


```


</details>


********

<details>
<summary>EXERCISE 3: Manually deploy new Docker Image on server
</summary>

```sh
# login to the droplet server
 ssh root@{droplet_server_ip}
 # login to docker using your credentials
docker login
# pull and run the node image from repo.
docker run -d -p 3000:3000 {docker_hub_repo/appname}:{image_name}
```

</details>

******

<details>
<summary>Extract all logic into Jenkins-shared-library with parameters and reference it in Jenkinsfile.</summary>

```sh
Go to jenkins--> Manage jenkins->system->global pipeline libraries-> add your shared library
 Please refer to **Jenkins** file in the project build automation and CICD with Jenkins.
 For jenkins shared library please copy and paste the below url in your browser. 
 https://gitlab.com/bootcamp-exercise/jenkins-shared-library
```

</details>
